
package com.yogi.controller;

import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/yogi")
@Slf4j
public class YogiController 
{
    // test endPoint,
    @GetMapping("/msg")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public String wishMsg()
    {
        return "\nWelcome to the Yogi Application.,";
    }
    
    @GetMapping("/getAge/{age}")
    @ResponseStatus(code = HttpStatus.VARIANT_ALSO_NEGOTIATES)
    public int getYourAge(int age)
    {
        LocalDate d = LocalDate.now();
        int year = d.getYear();
        log.info("Your age:"+age);
        return year-age;
    }
}
