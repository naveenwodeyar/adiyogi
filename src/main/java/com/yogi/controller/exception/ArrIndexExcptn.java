
package com.yogi.controller.exception;

import java.util.Arrays;

public class ArrIndexExcptn
{
    public static void arrExcptn() throws Exception
    {
        int[] numbers = {1,3,5,7};
        System.out.println("Array,"+Arrays.toString(numbers));
        System.out.println("Array size:"+numbers.length);
        System.out.println("Element at the location,"+numbers[5]);
    }
}
