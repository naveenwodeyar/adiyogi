
package com.yogi.controller.exception;

import java.util.logging.*;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ArithMeticExcpn 
{
    static void arithMeticExcpn() throws ArithmeticException
    {
        int a = 1;
        int b = 0+a;
        try
        {
            System.out.println("Arithametic Exception,"+a/b);           
        }
        catch(Exception e)
        {
            System.out.println(e.getLocalizedMessage());
            System.out.println("Idiot, go to School");
        }
    }
    
    public static void main(String[] args) 
    {
        try 
        {
            System.out.println("\n---**Arithmetic Exception**--\n");
            arithMeticExcpn();
            
            // composition,
            new ArrIndexExcptn().arrExcptn();
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(ArithMeticExcpn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
